# -*- coding: utf-8 -*-
from odoo import models, fields, api

class tiempo(models.Model):
    _name = 'mes.mes'

    _description = 'PERIODO LIBRO DE REMUNERACIONES'

    mes = fields.Selection([('1','Enero'),
                                 ('2','Febrero'),
                                 ('3','Marzo'),
                                 ('4','Abril'),
                                 ('5','Mayo'),
                                 ('6','Junio'),
                                 ('7','Julio'),
                                  ('8','Agosto'),
                                  ('9','Septiembre'),
                                  ('10','Octubre'),
                                  ('11','Noviembre'),
                                  ('12','Diciembre')
                                  ], tracking=True, index=True, string="MES LIBRO DE REMUNERACIONES")
    usuario_id = fields.Many2one('res.partner','Funcionario')


    #Datos de identificación del Trabajador (Cód 11xx)
    code_1102 = fields.Char('Fecha inicio contrato',tracking=True, index=True)
    code_1101 = fields.Char('Rut trabajador',tracking=True, index=True)
    code_1103 = fields.Char('Fecha término de contrato',tracking=True, index=True)
    code_1104 = fields.Selection([
                                    ('3','ART. 159 N°1: MUTUO ACUERDO DE LAS PARTES'),
                                    ('4','ART. 159 N°2: RENUNCIA DEL TRABAJADOR'),
                                    ('5','ART. 159 N°3: MUERTE DEL TRABAJADOR'),
                                    ('6','ART. 159 N°4: VENCIMIENTO DEL PLAZO CONVENIDO'),
                                    ('7','ART. 159 N°5: CONCLUSIÓN DEL TRABAJO O SERVICIO'),
                                    ('8','ART. 159 N°6: CASO FORTUITO O FUERZA MAYOR'),
                                    ('24','ART. 160 N°1 LETRA A: FALTA DE PROBIDAD'),
                                    ('25','ART. 160 N°1 LETRA B: CONDUCTAS DE ACOSO SEXUAL'),
                                    ('26','ART. 160 N°1 LETRA C: VÍAS DE HECHO'),
                                    ('27','ART. 160 N°1 LETRA D: INJURIAS'),
                                    ('28','ART. 160 N°1 LETRA E: CONDUCTA INMORAL'),
                                    ('29','ART. 160 N°1 LETRA F: CONDUCTAS DE ACOSO LABORAL'),
                                    ('11','ART. 160 N°2: NEGOCIACIONES PROHIBIDAS POR ESCRITO'),
                                    ('12','ART. 160 N°3: NO CONCURRENCIA A LAS LABORES'),
                                    ('13','ART. 160 N°4: ABANDONO DEL TRABAJO'),
                                    ('14','ART. 160 N°5: ACTOS, OMISIONES O IMPRUDENCIAS TEMERARIAS'),
                                    ('15','ART. 160 N°6: PERJUICIO MATERIAL CAUSADO INTENCIONALMENTE'),
                                    ('16','ART. 160 N°7: INCUMPLIMIENTO GRAVE DE LAS OBLIGACIONES'),
                                    ('18','ART. 161 INCISO PRIMERO: NECESIDADES DE LA EMPRESA'),
                                    ('19','ART. 161 INCISO SEGUNDO: DESAHUCIO ESCRITO DEL EMPLEADOR'),
                                    ('20','ART. 163 BIS: PROCEDIMIENTO CONCURSAL DE LIQUIDACIÓN')
                                     ], string='Causal término de contrato',tracking=True, index=True)



    code_1105 = fields.Char('Región prestación de servicios',tracking=True, index=True)
    code_1106 = fields.Char('Comuna prestación de servicios',tracking=True, index=True)
    code_1170 = fields.Selection([('1','IMPUESTO UNICO SEGUNDA CATEGORIA'),
                                 ('2','IMPUESTO UNICO OBRERO AGRICOLA'),
                                 ('3','IMPUESTO ADICIONAL')], string='Tipo impuesto a la renta',tracking=True, index=True)

    code_1146 = fields.Selection([('0','NO'),
                                  ('1','SI')
                                  ], string='Técnico extranjero exención cot. previsionales',tracking=True, index=True)





    code_1107 = fields.Selection([('101','ORDINARIA - ART 22'),
                                 ('201','PARCIAL- ART 40 BIS'),
                                 ('301','EXTRAORDINARIA ART 30'),
                                 ('401','ESPECIAL-ART 38 INCISO 5'),
                                 ('402','ESPECIAL-ART 23'),
                                 ('403','ESPECIAL-ART 106'),
                                 ('404','ESPECIAL-ART 152 TER D'),
                                 ('405','ESPECIAL-ART 152 TER F'),
                                 ('406','ESPECIAL-ART 25'),
                                 ('407','ESPECIAL-ART 25 BIS'),
                                 ('408','ESPECIAL-ART 149'),
                                 ('409','ESPECIAL-ART 149 INCISO 2'),
                                 ('410','ESPECIAL-ART 152 BIS'),
                                 ('411','ESPECIAL-ART 36 145-D'),
                                 ('412','ESPECIAL-ART 22 INCISO FINAL'),
                                 ('501','BISEMANAL- ART 149 INCISO 2'),
                                 ('601','JORNADA EXCEPCIONAL-ART 38 INCISO FINAL'),
                                 ('701','EXENTA- ART 22')],string='Código tipo de jornada',tracking=True, index=True)

    code_1108 = fields.Selection([('0','NO'),
                                 ('1','DISCAPACIDAD CERTIFICADA POR LA COMPIN'),
                                 ('2','ASIGNATARIO PENSION POR INVALIDEZ TOTAL'),
                                 ('3','PENSIONADO CON INVALIDEZ PARCIAL')], string='Persona con Discapacidad - Pensionado por Invalidez',tracking=True, index=True)



    code_1109 = fields.Selection([('0','NO'),
                                 ('1','SI')], string='Pensionado por vejez',tracking=True, index=True)





    code_1141 = fields.Selection([('100','NO ESTÁ EN AFP'),
                                   ('6','PROVIDA'),
                                   ('11','PLAN VITAL'),
                                   ('13','CUPRUM'),
                                   ('14','HABITAT'),
                                   ('19','UNO'),
                                   ('31','CAPITAL'),
                                   ('103','MODELO')],string='AFP',tracking=True, index=True)




    code_1142 = fields.Char('IPS  :(ExINP)',tracking=True, index=True)
    code_1143 = fields.Selection([('102','FONASA'),
                             ('1','CRUZ BLANCA'),
                             ('3','BANMEDICA'),
                             ('4','COLMENA'),
                             ('9','CONSALUD'),
                             ('12','VIDA TRES'),
                             ('37','CHUQUICAMATA'),
                             ('38','CRUZ DEL NORTE'),
                             ('39','FUSAT'),
                             ('40','FUNDACION (BANCO ESTADO)'),
                             ('41','RIO BLANCO'),
                              ('42','SAN LORENZO'),
                              ('43','NUEVA MAS VIDA')],string='FONASA - ISAPRE',tracking=True, index=True)


    code_1151 = fields.Selection([('0','NO'),
                                  ('1','SI')],string='AFC',tracking=True, index=True)



    code_1110 = fields.Selection([('0','NO'),
                             ('1','LOS ANDES'),
                             ('2','LA ARAUCANA'),
                             ('3','LOS HEROES'),
                             ('4','18 DE SEPTIEMBRE')],string='CCAF',tracking=True, index=True)


    code_1152 = fields.Selection([('0','SIN MUTUAL/ INSTITUTO SEGURIDAD LABORAL'),
                                  ('1','ASOCIACIÓN CHILENA DE SEGURIDAD (ACHS)'),
                                  ('2','MUTUAL SE SEGURIDAD CCHC'),
                                  ('3','INSTITUTO DE SEGURIDAD DEL TRABAJO (IST)')],string='Org. administrador ley 16.744',tracking=True, index=True)




    code_1111 = fields.Char('Nro cargas familiares legales autorizadas',tracking=True, index=True)
    code_1112 = fields.Char('Nro de cargas familiares maternales',tracking=True, index=True)
    code_1113 = fields.Integer('Nro de cargas familiares invalidez',tracking=True, index=True)
    code_1114 = fields.Selection([('A','PRIMER TRAMO'),
                            ('B','SEGUNDO TRAMO'),
                            ('C','TERCER TRAMO'),
                            ('D','SIN DERECHO'),
                            ('S','SIN INFORMACION')],string='Tramo asignación familiar',tracking=True, index=True)



    code_1171 = fields.Char('Rut org sindical 1',tracking=True, index=True)
    code_1172 = fields.Char('Rut org sindical 2',tracking=True, index=True)
    code_1173 = fields.Char('Rut org sindical 3',tracking=True, index=True)
    code_1174 = fields.Char('Rut org sindical 4',tracking=True, index=True)
    code_1175 = fields.Char('Rut org sindical 5',tracking=True, index=True)
    code_1176 = fields.Char('Rut org sindical 6',tracking=True, index=True)
    code_1177 = fields.Char('Rut org sindical 7',tracking=True, index=True)
    code_1178 = fields.Char('Rut org sindical 8',tracking=True, index=True)
    code_1179 = fields.Char('Rut org sindical 9',tracking=True, index=True)
    code_1180 = fields.Char('Rut org sindical 10',tracking=True, index=True)
    code_1115 = fields.Integer('Nro días trabajados en el mes',tracking=True, index=True)
    code_1116 = fields.Integer('Nro días de licencia médica en el mes',tracking=True, index=True)
    code_1117 = fields.Integer('Nro días de vacaciones en el mes',tracking=True, index=True)
    code_1118 = fields.Char('Subsidio trabajador joven',tracking=True, index=True)
    code_1154 = fields.Char('Puesto Trabajo Pesado',tracking=True, index=True)
    code_1155 = fields.Char('APVI',tracking=True, index=True)
    code_1157 = fields.Char('APVC',tracking=True, index=True)
    code_1131 = fields.Char('Indemnización a todo evento',tracking=True, index=True)
    code_1132 = fields.Char('Tasa indemnización a todo evento',tracking=True, index=True)
    #HABERES CÓD 2XXX
    #HABERES IMPONIBLES Y TRIBUTABLES (CÓD 21XX)
    code_2101 = fields.Integer('Sueldo',tracking=True, index=True)
    code_2102 = fields.Integer('Sobresueldo',tracking=True, index=True)
    code_2103 = fields.Integer('Comisiones',tracking=True, index=True)
    code_2104 = fields.Integer('Semana corrida',tracking=True, index=True)
    code_2105 = fields.Integer('Participación',tracking=True, index=True)
    code_2106 = fields.Integer('Gratificación',tracking=True, index=True)
    code_2107 = fields.Integer('Recargo 30% día domingo',tracking=True, index=True)
    code_2108 = fields.Integer('Remun. variable pagada en vacaciones',tracking=True, index=True)
    code_2109 = fields.Integer('Remun. variable pagada en clausura',tracking=True, index=True)
    code_2110 = fields.Integer('Aguinaldo',tracking=True, index=True)
    code_2111 = fields.Integer('Bonos u otras remun. fijas mensuales',tracking=True, index=True)
    code_2112 = fields.Integer('Tratos',tracking=True, index=True)
    code_2113 = fields.Integer('Bonos u otras remun. variables mensuales o superiores a un mes',tracking=True, index=True)
    code_2114 = fields.Integer('Ejercicio opción no pactada en contrato',tracking=True, index=True)
    code_2115 = fields.Integer('Beneficios en especie constitutivos de remun',tracking=True, index=True)
    code_2116 = fields.Integer('Remuneraciones bimestrales',tracking=True, index=True)
    code_2117 = fields.Integer('Remuneraciones trimestrales',tracking=True, index=True)
    code_2118 = fields.Integer('Remuneraciones cuatrimestral',tracking=True, index=True)
    code_2119 = fields.Integer('Remuneraciones semestrales',tracking=True, index=True)
    code_2120 = fields.Integer('Remuneraciones anuales',tracking=True, index=True)
    code_2121 = fields.Integer('Participación anual',tracking=True, index=True)
    code_2122 = fields.Integer('Gratificación anual',tracking=True, index=True)
    code_2123 = fields.Integer('Otras remuneraciones superiores a un mes',tracking=True, index=True)
    code_2124 = fields.Integer('Pago por horas de trabajo sindical',tracking=True, index=True)
    #HABERES IMPONIBLES Y NO TRIBUTABLES (CÓD 22XX)
    code_2201 = fields.Integer('Subsidio por incapacidad laboral por licencia médica',tracking=True, index=True)
    code_2202 = fields.Integer('Beca de estudio',tracking=True, index=True)
    code_2203 = fields.Integer('Gratificaciones de zona',tracking=True, index=True)
    code_2204 = fields.Integer('Otros ingresos no constitutivos de renta',tracking=True, index=True)
    #HABERES NO IMPONIBLES Y NO TRIBUTABLES (CÓD 23XX)
    code_2301 = fields.Integer('Colación',tracking=True, index=True)
    code_2302 = fields.Integer('Movilización',tracking=True, index=True)
    code_2303 = fields.Integer('Viáticos',tracking=True, index=True)
    code_2304 = fields.Integer('Asignación de pérdida de caja',tracking=True, index=True)
    code_2305 = fields.Integer('Asignación de desgaste herramienta',tracking=True, index=True)
    code_2311 = fields.Integer('Asignación familiar legal',tracking=True, index=True)
    code_2306 = fields.Integer('Gastos por causa del trabajo',tracking=True, index=True)
    code_2307 = fields.Integer('Gastos por cambio de residencia',tracking=True, index=True)
    code_2308 = fields.Integer('Sala cuna',tracking=True, index=True)
    code_2309 = fields.Integer('Asignación trabajo a distancia o teletrabajo',tracking=True, index=True)
    code_2347 = fields.Integer('Depósito convenido hasta UF 900',tracking=True, index=True)
    code_2310 = fields.Integer('Alojamiento por razones de trabajo',tracking=True, index=True)
    code_2312 = fields.Integer('Asignación de traslación',tracking=True, index=True)
    code_2313 = fields.Integer('Indemnización por feriado legal',tracking=True, index=True)
    code_2314 = fields.Integer('Indemnización años de servicio',tracking=True, index=True)
    code_2315 = fields.Integer('Indemnización sustitutiva del aviso previo',tracking=True, index=True)
    code_2316 = fields.Integer('Indemnización fuero maternal',tracking=True, index=True)
    code_2331 = fields.Integer('Pago indemnización a todo evento',tracking=True, index=True)
    #HABERES NO IMPONIBLES Y TRIBUTABLES (CÓD 24XX)
    code_2417 = fields.Integer('Indemnizaciones voluntarias tributables',tracking=True, index=True)
    code_2418 = fields.Integer('Indemnizaciones contractuales tributables',tracking=True, index=True)
    #DESCUENTOS COD 31XX
    code_3141 = fields.Integer('Cotización obligatoria previsional  :(AFP o IPS)',tracking=True, index=True)
    code_3143 = fields.Integer('Cotización obligatoria salud 7%',tracking=True, index=True)
    code_3144 = fields.Integer('Cotización voluntaria para salud',tracking=True, index=True)
    code_3151 = fields.Integer('Cotización AFC - trabajador',tracking=True, index=True)
    code_3146 = fields.Integer('Cotizaciones técnico extranjero para seguridad social fuera de Chile',tracking=True, index=True)
    code_3147 = fields.Integer('Descuento depósito convenido hasta UF 900 anual',tracking=True, index=True)
    code_3155 = fields.Integer('Cotización APVi Mod A',tracking=True, index=True)
    code_3156 = fields.Integer('Cotización APVi Mod B hasta UF50',tracking=True, index=True)
    code_3157 = fields.Integer('Cotización APVc Mod A',tracking=True, index=True)
    code_3158 = fields.Integer('Cotización APVc Mod B hasta UF50',tracking=True, index=True)
    code_3161 = fields.Integer('Impuesto retenido por remuneraciones',tracking=True, index=True)
    code_3162 = fields.Integer('Impuesto retenido por indemnizaciones',tracking=True, index=True)
    code_3163 = fields.Integer('Mayor retención de impuestos solicitada por el trabajador',tracking=True, index=True)
    code_3164 = fields.Integer('Impuesto retenido por reliquidación remun. devengadas otros períodos',tracking=True, index=True)
    code_3165 = fields.Integer('Diferencia impuesto reliquidación remun. devengadas en este período',tracking=True, index=True)
    code_3171 = fields.Integer('Cuota sindical 1',tracking=True, index=True)
    code_3172 = fields.Integer('Cuota sindical 2',tracking=True, index=True)
    code_3173 = fields.Integer('Cuota sindical 3',tracking=True, index=True)
    code_3174 = fields.Integer('Cuota sindical 4',tracking=True, index=True)
    code_3175 = fields.Integer('Cuota sindical 5',tracking=True, index=True)
    code_3176 = fields.Integer('Cuota sindical 6',tracking=True, index=True)
    code_3177 = fields.Integer('Cuota sindical 7',tracking=True, index=True)
    code_3178 = fields.Integer('Cuota sindical 8',tracking=True, index=True)
    code_3179 = fields.Integer('Cuota sindical 9',tracking=True, index=True)
    code_3180 = fields.Integer('Cuota sindical 10',tracking=True, index=True)
    code_3110 = fields.Integer('Crédito social CCAF',tracking=True, index=True)
    code_3181 = fields.Integer('Cuota vivienda o educación',tracking=True, index=True)
    code_3182 = fields.Integer('Crédito cooperativas de ahorro',tracking=True, index=True)
    code_3183 = fields.Integer('Otros descuentos autorizados y solicitados por el trabajador',tracking=True, index=True)
    code_3154 = fields.Integer('Cotización adicional trabajo pesado - trabajador',tracking=True, index=True)
    code_3184 = fields.Integer('Donaciones culturales y de reconstrucción',tracking=True, index=True)
    code_3185 = fields.Integer('Otros descuentos',tracking=True, index=True)
    code_3186 = fields.Integer('Pensiones de alimentos',tracking=True, index=True)
    code_3187 = fields.Integer('Descuento mujer casada',tracking=True, index=True)
    code_3188 = fields.Integer('Descuentos por anticipos y préstamos',tracking=True, index=True)
    #Aportes del Empleador (Cód 41xx)
    code_4151 = fields.Integer('AFC - Aporte empleador',tracking=True, index=True)
    code_4152 = fields.Integer('Aporte empleador seguro accidentes del trabajo y Ley SANNA',tracking=True, index=True)
    code_4131 = fields.Integer('Aporte empleador indemnización a todo evento',tracking=True, index=True)
    code_4154 = fields.Integer('Aporte adicional trabajo pesado - empleador',tracking=True, index=True)
    code_4155 = fields.Integer('Aporte empleador seguro invalidez y sobrevivencia',tracking=True, index=True)
    code_4157 = fields.Integer('APVC - Aporte Empleador',tracking=True, index=True)
    #Totales (Cód 5xxx)
    code_5201 = fields.Integer('Total haberes',tracking=True, index=True)
    code_5210 = fields.Integer('Total haberes imponibles y tributables',tracking=True, index=True)
    code_5220 = fields.Integer('Total haberes imponibles no tributables',tracking=True, index=True)
    code_5230 = fields.Integer('Total haberes no imponibles y no tributables',tracking=True, index=True)
    code_5240 = fields.Integer('Total haberes no imponibles y tributables',tracking=True, index=True)
    code_5301 = fields.Integer('Total descuentos',tracking=True, index=True)
    code_5361 = fields.Integer('Total descuentos impuestos a las remuneraciones',tracking=True, index=True)
    code_5362 = fields.Integer('Total descuentos impuestos por indemnizaciones',tracking=True, index=True)
    code_5341 = fields.Integer('Total descuentos por cotizaciones del trabajador',tracking=True, index=True)
    code_5302 = fields.Integer('Total otros descuentos',tracking=True, index=True)
    code_5410 = fields.Integer('Total aportes empleador',tracking=True, index=True)
    code_5501 = fields.Integer('Total líquido',tracking=True, index=True)
    code_5502 = fields.Integer('Total indemnizaciones',tracking=True, index=True)
    code_5564 = fields.Integer('Total indemnizaciones tributables',tracking=True, index=True)
    code_5565 = fields.Integer('Total indemnizaciones no tributables',tracking=True, index=True)
